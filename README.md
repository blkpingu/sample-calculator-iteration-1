# Sample Calculator

Sample Calculator is a command line tool to calculate characteristic values of a sample.

It provides the following features:
- Reading sample values from command line and CSV (Colon Separated Values) files.
- Performing statistic calculations such as average, variance, and deviation.
- Configurable logging of results and interim results.
- Easy integration of new input sources and calculations.

 **Please note:** The current version is only an initial alpha version which is **NOT** suited for production use.

this is a new line
Meaningful change
